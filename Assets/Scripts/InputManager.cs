﻿using UnityEngine;

public class InputManager
{
    private Kirby kirby;
    private GameManager gameManager;
    
    public void Initialize ()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        kirby = GameObject.FindGameObjectWithTag("Player").GetComponent<Kirby>();
	}	
	public void ReadInput ()
    {
        if(gameManager.gameover) return;

        if(Input.GetButtonDown("Cancel"))
        {
            gameManager.PauseGame();
        }

        if(gameManager.isPaused) return;

        if(Input.GetButtonDown("Fire1")) kirby.Fly();
        else if(Input.GetButtonUp("Fire1")) kirby.Fall();
	}
}
